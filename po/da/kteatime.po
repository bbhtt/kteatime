# translation of kteatime.po to
# Danish translation of kteatime
# Copyright (C).
#
# Erik Kjær Pedersen <erik@binghamton.edu>, 1999, 2002, 2003, 2004, 2005, 2006.
# Martin Schlander <mschlander@opensuse.org>, 2008, 2009, 2010, 2015, 2020, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kteatime\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-20 00:40+0000\n"
"PO-Revision-Date: 2022-02-08 20:01+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <dansk@dansk-gruppen.dk>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: src/main.cpp:27
#, kde-format
msgid "KTeaTime"
msgstr "KTeaTime"

#: src/main.cpp:29
#, kde-format
msgid "KDE utility for making a fine cup of tea."
msgstr "KDE-værktøj til at lave en god kop te."

#: src/main.cpp:31
#, kde-format
msgid ""
"© 1998-1999, Matthias Hölzer-Klüpfel\n"
"© 2002-2003, Martin Willers\n"
"© 2007-2010, Stefan Böhmann"
msgstr ""
"© 1998-1999, Matthias Hölzer-Klüpfel\n"
"© 2002-2003, Martin Willers\n"
"© 2007-2010, Stefan Böhmann"

#: src/main.cpp:39
#, kde-format
msgid "Stefan Böhmann"
msgstr "Stefan Böhmann"

#: src/main.cpp:40
#, kde-format
msgid "Current maintainer"
msgstr "Nuværende vedligeholder"

#: src/main.cpp:44
#, kde-format
msgid "Matthias Hölzer-Klüpfel"
msgstr "Matthias Hölzer-Klüpfel"

#: src/main.cpp:45
#, kde-format
msgid "Martin Willers"
msgstr "Martin Willers"

#: src/main.cpp:47
#, kde-format
msgid "Daniel Teske"
msgstr "Daniel Teske"

#: src/main.cpp:47
#, kde-format
msgid "Many patches"
msgstr "Mange patcher"

#: src/main.cpp:49
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Erik Kjær Pedersen,Martin Schlander"

#: src/main.cpp:49
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "erik@binghamton.edu,mschlander@opensuse.org"

#: src/main.cpp:64
#, kde-format
msgid "Start a new tea with this time."
msgstr "Start en ny te med dette tidspunkt."

#: src/main.cpp:64
#, kde-format
msgid "seconds"
msgstr "sekunder"

#: src/main.cpp:67
#, kde-format
msgid "Use this name for the tea started with --time."
msgstr "Brug dette navn til teen startet med --time."

#: src/main.cpp:68
#, kde-format
msgid "name"
msgstr "navn"

#. i18n: ectx: property (windowTitle), widget (QWidget, TimeEditWidget)
#: src/main.cpp:79 src/tea.cpp:25 src/timeedit.cpp:36 src/timeedit.cpp:104
#: src/timeedit.ui:14
#, kde-format
msgid "Custom Tea"
msgstr "Brugerdefineret te"

#: src/settings.cpp:39
#, kde-format
msgid "Configure Tea Cooker"
msgstr "Indstil tekogeren"

#: src/settings.cpp:52
#, kde-format
msgid "Save changes and close dialog."
msgstr "Gem ændringer og luk dialog."

#: src/settings.cpp:53
#, kde-format
msgid "Close dialog without saving changes."
msgstr "Luk dialog uden at gemme ændringer."

#: src/settings.cpp:54
#, kde-format
msgid "Show help page for this dialog."
msgstr "Vis hjælpeside for denne dialog."

#: src/settings.cpp:83
#, kde-format
msgctxt "Auto hide popup after"
msgid " second"
msgid_plural " seconds"
msgstr[0] " sekund"
msgstr[1] " sekunder"

#: src/settings.cpp:84
#, kde-format
msgctxt "Reminder every"
msgid " second"
msgid_plural " seconds"
msgstr[0] " sekund"
msgstr[1] " sekunder"

#. i18n: ectx: property (title), widget (QGroupBox, kbuttongroup1)
#: src/settings.ui:18
#, kde-format
msgid "Tea List"
msgstr "Te-liste"

#. i18n: ectx: property (title), widget (QGroupBox, teaPropertiesGroup)
#: src/settings.ui:112
#, kde-format
msgid "Tea Properties"
msgstr "Te-egenskaber"

#. i18n: ectx: property (text), widget (QLabel, label)
#: src/settings.ui:118 src/tealistmodel.cpp:93
#, kde-format
msgid "Name"
msgstr "Navn"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: src/settings.ui:132 src/tealistmodel.cpp:93
#, kde-format
msgid "Time"
msgstr "Tid"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: src/settings.ui:149
#, kde-format
msgid "min"
msgstr "min"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: src/settings.ui:166
#, kde-format
msgid "sec"
msgstr "sek"

#. i18n: ectx: property (title), widget (QGroupBox, kbuttongroup2)
#: src/settings.ui:189
#, kde-format
msgid "Action"
msgstr "Handling"

#. i18n: ectx: property (text), widget (QCheckBox, popupCheckBox)
#: src/settings.ui:195
#, kde-format
msgid "Popup"
msgstr "Popop"

#. i18n: ectx: property (text), widget (QCheckBox, autohideCheckBox)
#: src/settings.ui:226
#, kde-format
msgid "Auto hide popup after"
msgstr "Skjul automatisk popup efter"

#. i18n: ectx: property (text), widget (QCheckBox, reminderCheckBox)
#: src/settings.ui:275
#, kde-format
msgid "Reminder every"
msgstr "Påmindelse hver"

#. i18n: ectx: property (text), widget (QCheckBox, visualizeCheckBox)
#: src/settings.ui:322
#, kde-format
msgid "Visualize progress in icon tray"
msgstr "Visualisér fremskridt i statusikonen"

#: src/tea.cpp:56
#, kde-format
msgid "%1 year"
msgid_plural "%1 years"
msgstr[0] "%1 år"
msgstr[1] "%1 år"

#: src/tea.cpp:58
#, kde-format
msgid "%1 a"
msgid_plural "%1 a"
msgstr[0] "%1 a"
msgstr[1] "%1 a"

#: src/tea.cpp:68
#, kde-format
msgid "%1 day"
msgid_plural "%1 days"
msgstr[0] "%1 dag"
msgstr[1] "%1 dage"

#: src/tea.cpp:70
#, kde-format
msgid "%1 d"
msgid_plural "%1 d"
msgstr[0] "%1 d"
msgstr[1] "%1 d"

#: src/tea.cpp:80
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 time"
msgstr[1] "%1 timer"

#: src/tea.cpp:82
#, kde-format
msgid "%1 h"
msgid_plural "%1 h"
msgstr[0] "%1 h"
msgstr[1] "%1 h"

#: src/tea.cpp:92
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minut"
msgstr[1] "%1 minutter"

#: src/tea.cpp:94
#, kde-format
msgid "%1 min"
msgid_plural "%1 min"
msgstr[0] "%1 min"
msgstr[1] "%1 min"

#: src/tea.cpp:104
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekund"
msgstr[1] "%1 sekunder"

#: src/tea.cpp:106
#, kde-format
msgid "%1 s"
msgid_plural "%1 s"
msgstr[0] "%1 s"
msgstr[1] "%1 s"

#: src/tealistmodel.cpp:64
#, kde-format
msgid " ("
msgstr " ("

#: src/tealistmodel.cpp:66
#, kde-format
msgid ")"
msgstr ")"

#: src/tealistmodel.cpp:105
#, kde-format
msgid "Unnamed Tea"
msgstr "Unavngiven te"

#: src/timeedit.cpp:39
#, kde-format
msgid "Start a new custom tea with the time configured in this dialog."
msgstr ""
"Start en ny brugerdefineret te med den tid som er indstillet i denne dialog."

#: src/timeedit.cpp:42
#, kde-format
msgid "Close this dialog without starting a new tea."
msgstr "Luk denne dialog uden at starte en ny te."

#: src/timeedit.cpp:54
#, kde-format
msgid " minute"
msgid_plural " minutes"
msgstr[0] " minut"
msgstr[1] " minutter"

#: src/timeedit.cpp:55
#, kde-format
msgid " second"
msgid_plural " seconds"
msgstr[0] " sekund"
msgstr[1] " sekunder"

#. i18n: ectx: property (text), widget (QLabel, minutesLabel_2)
#: src/timeedit.ui:32
#, kde-format
msgid "Tea time:"
msgstr "Tetid:"

#: src/toplevel.cpp:50
#, kde-format
msgid "Unknown Tea"
msgstr "Ukendt te"

#: src/toplevel.cpp:60
#, kde-format
msgid "Black Tea"
msgstr "Sort te"

#: src/toplevel.cpp:61
#, kde-format
msgid "Earl Grey"
msgstr "Earl Grey"

#: src/toplevel.cpp:62
#, kde-format
msgid "Fruit Tea"
msgstr "Frugtte"

#: src/toplevel.cpp:63
#, kde-format
msgid "Green Tea"
msgstr "Grøn te"

#: src/toplevel.cpp:72
#, kde-format
msgid "&Resume"
msgstr "&Genoptag"

#: src/toplevel.cpp:78
#, kde-format
msgid "&Pause"
msgstr "&Pause"

#: src/toplevel.cpp:84
#, kde-format
msgid "&Stop"
msgstr "&Stop"

#: src/toplevel.cpp:90
#, kde-format
msgid "&Configure..."
msgstr "&Indstil..."

#: src/toplevel.cpp:98
#, kde-format
msgid "&Custom..."
msgstr "&Brugerdefineret..."

#: src/toplevel.cpp:154
#, kde-format
msgid "No steeping tea."
msgstr "Ingen te som trækker."

#: src/toplevel.cpp:207
#, kde-format
msgctxt "%1 - name of the tea, %2 - the predefined time for the tea"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: src/toplevel.cpp:285 src/toplevel.cpp:390
#, kde-format
msgid "The Tea Cooker"
msgstr "Tekogeren"

#: src/toplevel.cpp:291
#, kde-format
msgid "%1 is now ready!"
msgstr "Teen %1 er klar nu!"

#: src/toplevel.cpp:310
#, kde-format
msgid "%1 is ready since %2!"
msgstr "Teen %1 har været klar siden %2!"

#: src/toplevel.cpp:325
#, kde-format
msgctxt "%1 is the time, %2 is the name of the tea"
msgid "%1 left for %2."
msgstr "%1 tilbage for %2."

#~ msgid "Anonymous Tea"
#~ msgstr "Anonym te"

#~ msgid "&Anonymous..."
#~ msgstr "&Anonym..."

#~ msgid "Configure &amp;Notifications..."
#~ msgstr "Indstil &amp;bekendtgørelser..."

#~ msgid "Configure notifications"
#~ msgstr "Indstil bekendtgørelser"

#~ msgid "Notification"
#~ msgstr "Bekendtgørelse"

#~ msgid "N"
#~ msgstr "N"

#~ msgid "D"
#~ msgstr "D"

#~ msgid "U"
#~ msgstr "U"
